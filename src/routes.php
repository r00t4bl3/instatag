<?php
// Routes

$app->get('/', function ($req, $res, $args) {
    // Sample log message
    // $this->logger->info("Slim-Skeleton '/' route");

    $request_body = $req->getQueryParams();

    // var_dump($request_body);

    return $this->view->render($res, 'index.phtml', [
        'algolia' => $this->get('settings')['algolia'],
        'sitename' => $this->get('settings')['siteName'],
        'baseurl' => $this->get('settings')['baseUrl'],
        'keyword' => $request_body,
    ]);
});


$app->get('/about', function ($req, $res, $args) {
    return $this->view->render($res, 'about.phtml', [
        'sitename' => $this->get('settings')['siteName'],
        'baseurl' => $this->get('settings')['baseUrl'],
    ]);
});

$app->get('/submit-hashtag', function ($req, $res, $args) {
    return $this->view->render($res, 'submit-hashtag.phtml', [
        'sitename' => $this->get('settings')['siteName'],
        'baseurl' => $this->get('settings')['baseUrl'],
    ]);
});
