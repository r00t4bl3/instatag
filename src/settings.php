<?php
return [
    'settings' => [
        'siteName' => 'Who To Tag',
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header
        // 'baseUrl' => 'http://whototag.r00t4bl3.com/',
        'baseUrl' => 'http://localhost:8000/',

        // Algolia API key
        'algolia' => [
            'application_id'            => 'H8E6MXIT0F',
            'search_only_api_key'       => 'b5aefb6df65dd57031e21f1b3510c1f4',
            'admin_api_key'             => '6563f43afbf41268555a818c8ad25399',
            'index_name'                => 'test_hashtag',
        ],

        // Twig settings
        'view' => [
            'template_path' => __DIR__ . '/../templates/',
            'cache_path' => __DIR__ . '/../templates/cache/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],

    ],
];
