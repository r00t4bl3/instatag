<?php

namespace App\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


use GuzzleHttp\Psr7;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;


class RefreshIgLatestPost extends Command
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:refresh-ig-latest-post')

            // the short description shown while running "php bin/console list"
            ->setDescription('Refresh latest post on Algolia dataset.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command allows you refresh Algolia dataset with latest post.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $settings = require __DIR__ . '/../settings.php';

        $algolia_setting = $settings['settings']['algolia'];
        // var_dump($algolia_setting);
        // exit();
        $client     = new \AlgoliaSearch\Client($algolia_setting['application_id'], $algolia_setting['admin_api_key']);
        $index      = $client->initIndex($algolia_setting['index_name']);

        // $query      = 'topserbia'; // Empty query will match all records
        $query      = 'top'; // Empty query will match all records
        // $query      = 'topbarceloneresto'; // Empty query will match all records


        $client     = new Client;

        $i = 0;
        $ig_accounts    = [];
        // Fetching records from Algolia
        foreach ($index->browse($query) as $hit)
        {
            // Get IG account and save to array
            $ig_accounts[] = trim(urldecode($hit['account']));
            $i++;
        }

        $output->writeLn("Fetched ".$i." records from Algolia");

        foreach ($ig_accounts as $ig_account)
        {
            try {

                $ig_url     = "https://www.instagram.com/". $ig_account ."/?__a=1";
                $response   = $client->request('GET', $ig_url);

                $body       = $response->getBody();
                $content    = $body->getContents();
                $decoded    = json_decode($content);
                $thumbnail  = isset($decoded->user->media->nodes[0]->thumbnail_resources[0]->src) ? $decoded->user->media->nodes[0]->thumbnail_resources[0]->src : NULL;
                $preview    = isset($decoded->user->media->nodes[0]->thumbnail_resources[2]->src) ? $decoded->user->media->nodes[0]->thumbnail_resources[2]->src : NULL;

                // Update Algolia record
                $index->partialUpdateObject([
                    'objectID'  => $hit['objectID'],
                    'test'      => microtime(true),
                    'thumbnail_src' => $thumbnail,
                    'preview_src'   => $preview,
                ]);

                $output->writeLn("Fetched Instagram images for account ".$ig_account);


            } catch (RequestException $e) {
                if ($e->hasResponse()) {
                    $output->writeLn("Error for account ".$ig_account .": ".$e->getResponse()->getStatusCode());
                }
                else
                {
                    $output->writeLn("Error for account ".$ig_account);
                }

                // echo ;
                //     echo Psr7\str($e->getResponse());
                // }
            }


            // var_dump($hit);
        }

        $output->writeLn("Updating Algolia records done.");

    }




}
