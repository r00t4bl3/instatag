$( document ).ready(function() {
    var UNCODE = window.UNCODE || {};
    window.UNCODE = UNCODE;

    function menuMobile() {
        var $mobileToggleButton = $('.mobile-menu-button'),
            $box,
            $el,
            elHeight,
            check,
            animating = false,
            stickyMobile = false;
        UNCODE.menuOpened = false;
            //browserToolbar = 56;
        $mobileToggleButton.on('click', function(event) {
            var btn = this;
            if ($(btn).hasClass('overlay-close')) return;
            event.preventDefault();
            if (UNCODE.wwidth < UNCODE.mediaQuery) {
                $box = $(this).closest('.box-container').find('.main-menu-container'),
                    $el = $(this).closest('.box-container').find('.menu-horizontal-inner:not(.row-brand), .menu-sidebar-inner');
                if (UNCODE.isMobile && $('.menu-wrapper.menu-sticky, .menu-wrapper.menu-hide-only, .main-header .menu-sticky-vertical, .main-header .menu-hide-only-vertical').length) {
                    stickyMobile = true;
                    elHeight = window.innerHeight - UNCODE.menuMobileHeight - (UNCODE.bodyBorder * 2) - UNCODE.adminBarHeight + 1;
                } else {
                    elHeight = 0;
                    $.each($el, function(index, val) {
                        elHeight += $(val).outerHeight();
                    });
                }
                var open = function() {
                    if (!animating) {
                        animating = true;
                        UNCODE.menuOpened = true;
                        if ($('body[class*="vmenu-"], body.hmenu-center').length && ($('.menu-hide, .menu-sticky').length)) {
                            $('.main-header > .vmenu-container').css({position:'fixed', top: ($('.menu-container').outerHeight() + UNCODE.bodyBorder + UNCODE.adminBarHeight) + 'px'});
                            $('.menu-container:not(.sticky-element):not(.isotope-filters)').css({position:'fixed'});
                        }
                        if ($('body.hmenu-center').length && ($('.menu-hide, .menu-sticky').length)) {
                            $('.menu-container:not(.sticky-element):not(.isotope-filters)').css({position:'fixed', top: (UNCODE.menuMobileHeight + UNCODE.bodyBorder + UNCODE.adminBarHeight) + 'px'});
                        }
                        btn.classList.add('close');
                        $box.addClass('open-items');
                        $box.animate({
                            height: elHeight
                        }, 600, "easeInOutCirc", function() {
                            animating = false;
                            if (!stickyMobile) $box.css('height', 'auto');
                        });
                    }
                };

                var close = function() {
                    if (!animating) {
                        animating = true;
                        UNCODE.menuOpened = false;
                        btn.classList.remove('close');
                        btn.classList.add('closing');
                        $box.addClass('close');
                        setTimeout(function() {
                            $box.removeClass('close');
                            $box.removeClass('open-items');
                            btn.classList.remove('closing');
                        }, 500);
                        $box.animate({
                            height: 0
                        }, {
                            duration: 600,
                            easing: "easeInOutCirc",
                            complete: function(elements) {
                                $(elements).css('height', '');
                                animating = false;
                                if ($('body[class*="vmenu-"]').length) $('.main-header > .vmenu-container').css('position','relative');
                            }
                        });
                    }
                };
                check = (!UNCODE.menuOpened) ? open() : close();
            }
        });
        window.addEventListener('menuMobileTrigged', function(e) {
            $mobileToggleButton.trigger('click');
        });
        window.addEventListener("resize", function() {
            if ($(window).width() < UNCODE.mediaQuery) {
                if (UNCODE.isMobile) {
                    var $box = $('.box-container .main-menu-container'),
                        $el = $('.box-container .menu-horizontal-inner, .box-container .menu-sidebar-inner');
                    if ($($box).length && $($box).hasClass('open-items') && $($box).css('height') != 'auto') {
                        if ($('.menu-wrapper.menu-sticky, .menu-wrapper.menu-hide-only').length) {
                            elHeight = 0;
                            $.each($el, function(index, val) {
                                elHeight += $(val).outerHeight();
                            });
                            elHeight = window.innerHeight - $('.menu-wrapper.menu-sticky .menu-container .row-menu-inner, .menu-wrapper.menu-hide-only .menu-container .row-menu-inner').height() - (UNCODE.bodyBorder * 2) + 1;
                            $($box).css('height', elHeight + 'px');
                        }
                    }
                }
            } else {
                $('.menu-hide-vertical').removeAttr('style');
                $('.menu-container-mobile').removeAttr('style');
                $('.vmenu-container.menu-container').removeAttr('style');
            }
        });
    };

    function menuOffCanvas() {
        $('.menu-primary .menu-button-offcanvas').click(function(event) {
            if ($(window).width() > UNCODE.mediaQuery) {
                if ($(event.currentTarget).hasClass('close')) {
                    $(event.currentTarget).removeClass('close');
                    $(event.currentTarget).addClass('closing');
                    setTimeout(function() {
                        $(event.currentTarget).removeClass('closing');
                    }, 500);
                } else $(event.currentTarget).addClass('close');
            }
            $('body').toggleClass('off-opened');
        });
    };
    function menuSmart() {
        if ($('[class*="menu-smart"]').length > 0) {
            $('[class*="menu-smart"]').smartmenus({
                subIndicators: false,
                subIndicatorsPos: 'append',
                subMenusMinWidth: '13em',
                subIndicatorsText: '',
                showTimeout: 50,
                hideTimeout: 50,
                showFunction: function($ul, complete) {
                    $ul.fadeIn(0, 'linear', complete);
                },
                hideFunction: function($ul, complete) {
                    var fixIE = $('html.ie').length;
                    if (fixIE) {
                        var $rowParent = $($ul).closest('.main-menu-container');
                        $rowParent.height('auto');
                    }
                    $ul.fadeOut(0, 'linear', complete);
                },
                collapsibleShowFunction: function($ul, complete) {
                    $ul.slideDown(400, 'easeInOutCirc', function() {
                12});
                },
                collapsibleHideFunction: function($ul, complete) {
                    $ul.slideUp(200, 'easeInOutCirc', complete);
                },
                hideOnClick: true
            });
        }
    };
    function menuOverlay() {
        if ($('.overlay-sequential, .menu-mobile-animated').length > 0) {
            $('.overlay-sequential .menu-smart > li, .menu-sticky .menu-container .menu-smart > li, .menu-hide.menu-container .menu-smart > li, .vmenu-container .menu-smart > li').each(function(index, el) {
                var transDelay = (index / 20) + 0.1;
                $(this)[0].setAttribute('style', '-webkit-transition-delay:' + transDelay + 's; -moz-transition-delay:' + transDelay + 's; -ms-transition-delay:' + transDelay + 's; -o-transition-delay:' + transDelay + 's; transition-delay:' + transDelay + 's');
            });
        }
    };
    //menuMobileButton();
    menuMobile();
    menuOffCanvas();
    menuSmart();
    menuOverlay();
})
